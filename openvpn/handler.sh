#!/bin/sh

# Copyright (C) 2014-2016 Lorraine Data Network
# Copyright (C) 2014-2018 Alsace Réseau Neutre
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.


# Debian GNU/Linux: store this in /etc/openvpn/


if [ "$(id -ru)" = "0" ]; then
    SUDO=
else
    SUDO=sudo
fi


###### Utility
log() {
    local level
    level="$1"
    shift
    logger -t"ovpn-script[$$]" -pdaemon."$level" -- "$@"
}


###### Functions
# Load user information from /etc/openvpn/users/$common_name
get_user_info() {
    if ! echo "$common_name" | grep '^[a-zA-Z][a-zA-Z0-9_-]*$'; then
        log notice "Bad common name $common_name"
        return 1
    fi

    if ! . "/etc/openvpn/users/$common_name" ; then
        log notice "No configuration for user $common_name"
        return 1
    fi
}


# Write user specific OpenVPN configuration to stdout
create_conf() {
    if ! [ -z "$IP4" ]; then
        echo "ifconfig-push $IP4 $ifconfig_netmask"
    fi

    if ! [ -z "$IP6" ]; then
        echo "ifconfig-ipv6-push $IP6/64 $ifconfig_ipv6_local"
    fi

    if ! [ -z "$PREFIX" ]; then
        # Route the IPv6 delegated prefix:
        echo "iroute-ipv6 $PREFIX"
        # Set the OPENVPN_DELEGATED_IPV6_PREFIX in the client:
        echo "push \"setenv-safe DELEGATED_IPV6_PREFIX $PREFIX\""
    fi

    if ! [ -z "$PREFIX4" ]; then
        # Route the IPv4 delegated prefix:
        echo "iroute $PREFIX4" | tr '/' ' '
    fi
}


add_route() {
    $SUDO ip route replace "$@"
}


# Add the routes for the user in the kernel
add_routes() {
    if ! [ -z "$IP4" ]; then
        log info "Adding IPv4 $IP4 for $common_name"
        add_route $IP4/32 dev $dev protocol static && log info "Adding IPv4 $IP4 for $common_name: OK"
    fi

    if ! [ -z "$IP6" ]; then
        log info "Adding IPv6 $IP6 for $common_name"
        add_route $IP6/128 dev $dev protocol static && log info "Adding IPv6 $IP6 for $common_name: OK"
    fi

    if ! [ -z "$PREFIX" ]; then
        log info "Adding IPv6 delegated prefix $PREFIX for $common_name"
        add_route $PREFIX via $IP6 dev $dev protocol static && log info "Adding IPv6 delegated prefix $PREFIX for $common_name: OK"
    fi

    if ! [ -z "$PREFIX4" ]; then
        log info "Adding IPv4 delegated prefix $PREFIX4 for $common_name"
        add_route $PREFIX4 via $IP4 dev $dev protocol static && log info "Adding IPv4 delegated prefix $PREFIX4 for $common_name: OK"
    fi
}


remove_routes() {
    if ! [ -z "$PREFIX4" ]; then
        $SUDO ip route del $PREFIX4 via $IP4 dev $dev protocol static
    fi

    if ! [ -z "$IP4" ]; then
        $SUDO ip route del $IP4/32 dev $dev protocol static
    fi

    if ! [ -z "$PREFIX" ]; then
        $SUDO ip route del $PREFIX via $IP6 dev $dev protocol static
    fi

    if ! [ -z "$IP6" ]; then
        $SUDO ip route del $IP6/128 dev $dev protocol static
    fi
}


set_routes() {
    if ! add_routes; then
        remove_routes
        return 1
    fi
}


###### OpenVPN handlers
client_connect() {
    conf="$1"
    get_user_info || exit 1
    create_conf > "$conf"
    set_routes
}


client_disconnect() {
    get_user_info || exit 1
    remove_routes
}

###### Dispatch
log info "OpenVPN $script_type $@"
case "$script_type" in
    client-connect)    client_connect "$@" ;;
    client-disconnect) client_disconnect "$@" ;;
esac
