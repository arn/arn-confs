#!/bin/bash

# Copyright (C) 2016-2018 Alsace Réseau Neutre
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

#
# This script facilitates the management of ARN's VPNs.
#
# Debian GNU/Linux: store this where you want. For example: /usr/local/sbin/


show_usage() {

  echo -e "\nError: $1\n" >&2

  cat <<EOF >&2
Usage:  $(basename $0) action [-l login] [-4 IPv4] [-6 IPv6interco] [-p IPv6prefix]

  action              What to do: create, remove, renew, list, show       (required)
  -l <login>          Username corresponding to the VPN to manage         (non-list)
  -4 <ipv4>           IPv4 address                                        (create-only)
  -6 <ipv6>           Interconnect IPv6 address                           (create-only)
  -p <ipv6>           Delegated IPv6 prefix                               (create-only)

EOF

  exit 1
}

action=$1
shift

while getopts "l:4:6:p:" opt; do
  case $opt in
    l) login="$OPTARG" ;;
    4) ipv4="$OPTARG" ;;
    6) ipv6="$OPTARG" ;;
    p) prefix=$OPTARG ;;
  esac
done

# check inputs
case $action in
  create)
    # should be sufficient…
    [[ "$ipv4" =~   ^([0-9]{1,3}.){3}[0-9]{1,3}( |$) ]]               || show_usage 'Invalid IPv4'
    [[ "$ipv6" =~   ^([0-9a-fA-F]{0,4}:){1,7}[0-9a-fA-F]{1,4}( |$) ]] || show_usage 'Invalid IPv6'
    [[ "$prefix" =~ ^([0-9a-fA-F]{0,4}:){1,8}/[0-9]{1,3}( |$) ]]      || show_usage 'Invalid IPv6 prefix'
  ;;&
  remove)
  ;;&
  renew)
  ;;&
  show)
  ;;&
  create|remove|renew|show)
    [[ "$login" =~ ^[a-z0-9-]+$ ]]  || show_usage 'Bad login (only [a-z0-9-])'
  ;;
  list)
  ;;
  *)
    show_usage "Unknown action $action"
  ;;
esac

cd /etc/openvpn/easy-rsa
. ./vars > /dev/null

function createcrypto
{
  ./build-key "$login"
  mkdir "/tmp/$login"
  cp "keys/$login".{crt,key} keys/ca.crt "/tmp/$login"
  zip -q -r "$login.zip" "/tmp/$login"
  rm -r "/tmp/$login"
  rm keys/"$login".{csr,key}

  echo "Archive to transmit is $PWD/$login.zip. Remove it afterwards."
}

function revoke
{
  ./revoke-full "$login"
  cp /etc/openvpn/easy-rsa/keys/crl.pem /etc/openvpn/crypto/crl.pem
  rm "/etc/openvpn/easy-rsa/keys/$login".*
}

function cleanup
{
  rm "/etc/openvpn/users/$login"
}

function createdatafile
{
  cat <<EOF >"/etc/openvpn/users/$login"
IP4="$ipv4"
IP6="$ipv6"
PREFIX="$prefix"
EOF
}

function showinfos
{
  . "/etc/openvpn/users/$login"
  cat <<EOF
  Infos VPN ARN $login
  
  Adresse IPv4          : $IP4
  Adresse IPv6          : $IP6
  Préfixe IPv6 délégué  : $PREFIX
  Reverses IP           : aucun
EOF
}

case $action in
  create)
    createcrypto
    createdatafile
    showinfos
  ;;
  remove)
    revoke
    cleanup
  ;;
  renew)
    revoke
    createcrypto
  ;;
  show)
    showinfos
  ;;
  list)
    CERTS=( $(cd keys/; ls *.crt | grep -v -e ca.crt -e server.crt) )
    printf '%s\n' "${CERTS[@]}"
    echo -e "\nTotal: ${#CERTS[@]}"
  ;;
esac
