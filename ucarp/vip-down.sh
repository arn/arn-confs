#!/bin/sh

# Copyright (C) 2015-2018 Alsace Réseau Neutre
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.


# Debian GNU/Linux: store this where you want. For example: /etc/carp/


IPBIN="/sbin/ip"
VLANIDREGEX="[58][0-9]{2}"

CARPIF=$1
VIPv4=$2
VIPv6=$3
IFLIST=$($IPBIN -d l | grep -B2 -E "802.1Q id $VLANIDREGEX" | grep -Po "h-[a-z]+(?=@)")

for iface in $IFLIST
do
    echo "[INFO] Deleting VIP $VIPv4 from interface $iface"
    $IPBIN a d $VIPv4/24 scope link dev $iface

    echo "[INFO] Deleting VIP $VIPv6 from interface $iface"
    $IPBIN -6 a d $VIPv6/112 scope link dev $iface
done

