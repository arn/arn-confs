#!/bin/sh

# Copyright (C) 2015-2018 Alsace Réseau Neutre
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.


# Debian GNU/Linux: store this where you want. For example: /etc/carp/


# Required softwares: arping (debian package "iputils-arping") 
# and atk6-fake_advertise6 (debian package "thc-ipv6")


IPBIN="/sbin/ip"
ARPPINGBIN="/usr/bin/arping"
FAKEADVBIN="/usr/bin/atk6-fake_advertise6"
VLANIDREGEX="[58][0-9]{2}"

CARPIF=$1
VIPv4=$2
VIPv6=$3
IFLIST=$($IPBIN -d l | grep -B2 -E "802.1Q id $VLANIDREGEX" | grep -Po "h-[a-z]+(?=@)")


if [ ! -x $ARPPINGBIN ] || [ ! -x $FAKEADVBIN ]; then
    echo "[ERROR] $ARPPINGBIN (debian package \"iputils-arping\") or $FAKEADVBIN (debian package \"thc-ipv6\") not installed or not executable!"
    exit 1
fi


for iface in $IFLIST
do
    echo "[INFO] Adding VIP $VIPv4 on interface $iface"
    $IPBIN a a $VIPv4/24 scope link dev $iface

    echo "[INFO] Flooding gratuitous ARP for VIP $VIPv4 on interface $iface"
    $ARPPINGBIN -I $iface -c 10 -q -U $VIPv4 1>/dev/null &

    echo "[INFO] Adding VIP $VIPv6 on interface $iface"
    $IPBIN -6 a a $VIPv6/112 scope link dev $iface

    echo "[INFO] Flooding unsolicited NDP advertisement for VIP $VIPv6 on interface $iface"
    $FAKEADVBIN -n 10 -w 1 $iface $VIPv6 1>/dev/null &
done

